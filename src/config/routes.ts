import { NodeController } from "../controllers/node.controller";

export class Routes {
    public nodesController: NodeController = new NodeController();

    public routes(app): void {
        app
            .route("/").get(this.nodesController.index);

        app
            .route("/nodes")
            .get(this.nodesController.index)
            .post(this.nodesController.create);

        app
            .route("/nodes/:id")
            .get(this.nodesController.show)
            .put(this.nodesController.update)
            .delete(this.nodesController.delete);
    }
}