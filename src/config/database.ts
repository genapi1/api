import { Sequelize } from "sequelize";
import { dotenv } from "./environment";

export const database = new Sequelize({
    host: dotenv.parsed.DB_HOST || 'localhost',
    database: dotenv.parsed.DB_NAME || 'genapi',
    username: dotenv.parsed.DB_USERNAME || 'root',
    password: dotenv.parsed.DB_PASSWORD || 'root',
    dialect: "mysql"
});